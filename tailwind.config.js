module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      width: {
        "1/7": "15%",
        "6/7": "85%",
        14: "4.3rem",
      },
      colors: {
        primary: "#7367f0",
        secondary: "#7267f0b2",
      },
    },
  },
  plugins: [],
};
