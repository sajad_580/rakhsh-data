import MyService from "../components/Home/MyService";
import PrimeryServices from "../components/Home/PrimaryServices";
import SelectedServices from "../components/Home/SelectedServices";

const HomePage = () => {
  return (
    <div className="w-full">
      <MyService />
      <PrimeryServices />
      <SelectedServices />
    </div>
  );
};

export default HomePage;
