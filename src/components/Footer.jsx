import { NavLink } from "react-router-dom";

const Footer = ({ links }) => {
  return (
    <div className="w-full flex fixed bottom-0 bg-opacity-10 backdrop-blur-md">
      <ul className="w-full flex flex-wrap items-center justify-between mx-5 py-5">
        {links.map((link, index) => (
          <li key={index} className="text-2xl md:text-3xl">
            <NavLink
              to={link.path}
              className={(navData) =>
                navData.isActive ? "text-primary border-b-2 border-primary" : ""
              }
            >
              {link.icon}
            </NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Footer;
