import { FaCogs, FaBook, FaPlus } from "react-icons/fa";

const services = [
  {
    name: ".........",
    icon: <FaBook size={25} className="text-primary" />,
  },
  {
    name: ".........",
    icon: <FaBook size={25} className="text-primary" />,
  },
  {
    name: ".........",
    icon: <FaBook size={25} className="text-primary" />,
  },
];

const SelectedServices = () => {
  return (
    <div className="w-full relative rounded-md border border-gray-200 shadow-md px-5 py-10 my-24 lg:my-16">
      <p className="absolute -top-5 right-0 flex items-center justify-around rounded-md bg-primary py-1 w-40 text-white">
        <FaCogs />
        <span>خدمات انتخابی</span>
      </p>

      <div className="grid grid-cols-1 md:grid-cols-3 gap-x-3 gap-y-5 my-5">
        {services.map((service, index) => (
          <div
            key={index}
            className="flex flex-col items-center justify-between bg-gray-50 border border-gray-200 rounded-lg py-3 shadow-md"
          >
            <button className="w-12 h-12 rounded-full flex items-center justify-center bg-purple-100">
              {service.icon}
            </button>

            <span className="mt-5">{service.name}</span>
          </div>
        ))}
      </div>

      <button className="w-8 h-8 absolute left-5 bottom-3 flex items-center justify-center rounded-full bg-primary text-white">
        <FaPlus />
      </button>
    </div>
  );
};

export default SelectedServices;
