import { FaCogs, FaBook } from "react-icons/fa";

const services = [
  {
    name: "ثبت سوخت خودرو",
    icon: <FaBook size={25} className="text-primary" />,
  },
  {
    name: "ثبت هزینه",
    icon: <FaBook size={25} className="text-primary" />,
  },
  {
    name: "ثبت کارکرد خودرو",
    icon: <FaBook size={25} className="text-primary" />,
  },
];

const PrimeryServices = () => {
  return (
    <div className="w-full relative rounded-md border border-gray-200 shadow-md px-5 py-10 my-24 lg:my-16">
      <p className="absolute -top-5 right-0 flex items-center justify-around rounded-md bg-primary py-1 w-40 text-white">
        <FaCogs />
        <span>خدمات اصلی</span>
      </p>

      <div className="grid grid-cols-1 md:grid-cols-3 gap-x-3 gap-y-5 my-5">
        {services.map((service, index) => (
          <div
            key={index}
            className="flex flex-col items-center justify-between bg-gray-50 border border-gray-200 rounded-lg py-3 shadow-md"
          >
            <button className="w-12 h-12 rounded-full flex items-center justify-center bg-purple-100">
              {service.icon}
            </button>

            <span className="mt-5">{service.name}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default PrimeryServices;
