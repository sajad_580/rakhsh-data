import { FaCarAlt, FaRegMap } from "react-icons/fa";

const items = [
  {
    name: "مسیریابی به تعمیرگاه",
    icon: <FaRegMap size={25} className="text-primary" />,
    btn: null,
  },
  {
    name: "خودروی من",
    icon: <FaCarAlt size={25} className="text-primary" />,
    btn: "لیست خودروی جدید",
  },
];

const MyService = () => {
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-x-3 gap-y-5 mt-20 mb-24">
      {items.map((item, index) => (
        <div
          key={index}
          className="relative flex flex-col items-center justify-between bg-gray-50 border border-gray-200 rounded-lg py-3 shadow-md"
        >
          <button className="w-12 h-12 rounded-full flex items-center justify-center bg-purple-100">
            {item.icon}
          </button>
          <span className="mt-5">{item.name}</span>
          {item.btn && (
            <button className="absolute -bottom-8 py-1 px-3 rounded-md bg-primary text-white">
              {item.btn}
            </button>
          )}
        </div>
      ))}
    </div>
  );
};

export default MyService;
