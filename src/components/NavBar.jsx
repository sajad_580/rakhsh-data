import { FaListUl, FaUserCircle } from "react-icons/fa";

const NavBar = () => {
  return (
    <div className="w-full backdrop-blur-2xl p-3 md:p-5">
      <div className="flex items-center justify-between rounded-md bg-primary text-white text-2xl p-5">
        <button>
          <FaListUl />
        </button>
        <button>
          <FaUserCircle />
        </button>
      </div>
    </div>
  );
};

export default NavBar;
