import { NavLink } from "react-router-dom";
import { FaRegCircle, FaRegDotCircle } from "react-icons/fa";

const SideBar = ({
  links,
  fullSideBar,
  minimalSideBar,
  ClickSideBarHandler,
}) => {
  return (
    <div className="w-full bg-white">
      <section className="mb-3 px-3">
        <div className="w-full h-16 flex items-center border-b border-gray-200">
          <img
            src="/logo192.png"
            alt="rakhsh-logo"
            width="40"
            height="40"
            className="rounded-full"
          />
          {!minimalSideBar && (
            <div className="w-full flex items-center">
              <NavLink
                to="/"
                className="text-xl font-bold mr-5 whitespace-nowrap"
              >
                رخش دیتا
              </NavLink>
              <button
                className="hidden lg:block mr-auto"
                onClick={ClickSideBarHandler}
              >
                {fullSideBar ? (
                  <FaRegDotCircle size={17} />
                ) : (
                  <FaRegCircle size={17} />
                )}
              </button>
            </div>
          )}
        </div>
      </section>

      <section className="px-2">
        <ul>
          {links.map((link, index) => (
            <li key={index}>
              <NavLink
                to={link.path}
                className={(navData) =>
                  navData.isActive
                    ? `${
                        minimalSideBar ? "pr-4" : "p-4"
                      } sideBarLink sideBarActiveLink shadow-md`
                    : `${
                        minimalSideBar ? "pr-4" : "p-4"
                      } sideBarLink hover:text-primary transition-all hover:mr-1 duration-400`
                }
              >
                <p className="ml-3 text-lg">{link.icon}</p>
                {!minimalSideBar && (
                  <p className="whitespace-nowrap">{link.name}</p>
                )}
              </NavLink>
            </li>
          ))}
        </ul>
      </section>
    </div>
  );
};

export default SideBar;
