import HomePage from "./pages/HomePage";
import RepairShopPage from "./pages/RepairShopPage";
import VehiclePage from "./pages/VehiclePage";
import CostRegistrationPage from "./pages/CostRegistrationPage";

const routes = [
  { path: "/", element: <HomePage /> },
  { path: "/repair-shop", element: <RepairShopPage /> },
  { path: "/vehicle", element: <VehiclePage /> },
  { path: "cost-registration", element: <CostRegistrationPage /> },
];

export default routes;
