import { Routes, Route } from "react-router-dom";
import routes from "./routes";

import Layout from "./layout/Layout";

function App() {
  return (
    <Layout>
      <Routes>
        {routes.map((route, index) => (
          <Route key={index} {...route} />
        ))}
      </Routes>
    </Layout>
  );
}

export default App;
