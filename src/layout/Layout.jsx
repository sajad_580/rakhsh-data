import { useEffect, useState } from "react";
import { FaHome, FaCog, FaCarAlt, FaDollarSign } from "react-icons/fa";

import NavBar from "../components/NavBar";
import SideBar from "../components/SideBar";
import Footer from "../components/Footer";

const links = [
  { name: "خانه", path: "/", icon: <FaHome /> },
  { name: "تعمیرگاه", path: "repair-shop", icon: <FaCog /> },
  { name: "ماشین من", path: "vehicle", icon: <FaCarAlt /> },
  { name: "هزینه ها", path: "cost-registration", icon: <FaDollarSign /> },
];

const Layout = ({ children }) => {
  const [fullSideBar, setFullSideBar] = useState(true);
  const [minimalSideBar, setMinimalSideBar] = useState(false);
  const [sideBarWidth, setSideBarWidth] = useState("w-72");

  const ClickSideBarHandler = () => {
    setMinimalSideBar(fullSideBar);
    setFullSideBar(!fullSideBar);
  };

  const hoverSideBarHandler = (e) => {
    if (!fullSideBar) {
      if (e.type === "mouseover") {
        setMinimalSideBar(false);
        setSideBarWidth("w-72");
      } else {
        setMinimalSideBar(true);
        setSideBarWidth("w-14");
      }
    }
  };

  useEffect(() => {
    if (!fullSideBar) {
      setSideBarWidth("w-14");
    }
  }, [fullSideBar]);

  return (
    <div dir="rtl">
      <section className="flex items-start w-full">
        {/* sideBar section */}
        <div
          className={`${sideBarWidth} fixed min-h-screen hidden lg:flex h-full border-l border-gray-200 transition-all duration-300 z-30`}
          onMouseOver={hoverSideBarHandler}
          onMouseLeave={hoverSideBarHandler}
        >
          <SideBar
            links={links}
            fullSideBar={fullSideBar}
            minimalSideBar={minimalSideBar}
            ClickSideBarHandler={ClickSideBarHandler}
          />
        </div>

        <div
          className={`w-full bg-white transition-all duration-300 ${
            fullSideBar || !minimalSideBar ? "lg:mr-72" : "lg:mr-16"
          }`}
        >
          {/* navBar section */}
          <div
            className={`fixed z-20 flex items-center transition-all duration-300 ${
              fullSideBar || !minimalSideBar
                ? "w-full lg:w-[calc(100%_-_18rem)]"
                : "w-full lg:w-[calc(100%_-_4rem)]"
            }`}
          >
            <NavBar />
          </div>

          {/* layout children */}
          <div className="w-full px-3 md:px-5 mt-24 md:mt-28">{children}</div>
        </div>
      </section>

      {/* footer section */}
      <section className="lg:hidden">
        <Footer links={links} />
      </section>
    </div>
  );
};

export default Layout;
